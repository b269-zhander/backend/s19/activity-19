// console.log("Hello")



/*
    1. Declare 3 variables without initialization called username,password and role.
*/


/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/

/*const name = prompt("Enter your username");
if (name === null || name === "") {
  alert("You did not enter a username.");
}

const password = prompt("Enter your password");
if (password === null || password === "") {
  alert("You did not enter a password.");
}

const role = prompt("Enter your role");
if (role === null || role === "") {
  alert("You did not enter a role.");
}*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

function checkGrade(grade) {
	if(grade < 75) {
		return 'F';
	}

	else if (grade >= 75 && grade <= 80) {
		return 'D';
	}
	else if (grade >= 80 && grade <= 85) {
		return 'C';
	}
	else if (grade >= 85 && grade <= 90) {
		return 'B';
	}
	else if (grade >= 90 && grade <= 95) {
		return 'A';
	}
	else if (grade >= 95 && grade <= 100) {
		return 'A+';
	}
	
}

function getAverage(num1,num2,num3,num4){
	return (num1+num2+num3+num4)/4
}

let averageVar = getAverage(70,70,72,71);

console.log("checkAverage"+ "(70,70,72,71)");

letter = checkGrade(70)

console.log("Hello, student, your average is: " + averageVar  + ". The letter equivalent is " + letter)



let averageVar2 = getAverage(76,76,77,79);

console.log("checkAverage"+ "(76,76,77,79)");

letter2 = checkGrade(77)

console.log("Hello, student, your average is: " + averageVar2  + ". The letter equivalent is " + letter2)




let averageVar3 = getAverage(81,83,84,85);

console.log("checkAverage"+ "(81,83,84,85)");

letter3 = checkGrade(83)

console.log("Hello, student, your average is: " + averageVar3  + ". The letter equivalent is " + letter3)



let averageVar4 = getAverage(87,88,88,89);

console.log("checkAverage"+ "(87,88,88,89)");

letter4 = checkGrade(88)

console.log("Hello, student, your average is: " + averageVar4  + ". The letter equivalent is " + letter4)



let averageVar5 = getAverage(91,90,92,90);

console.log("checkAverage"+ "(91,90,92,90)");

letter5 = checkGrade(91)

console.log("Hello, student, your average is: " + averageVar5  + ". The letter equivalent is " + letter5)




let averageVar6 = getAverage(96,95,92,90);

console.log("checkAverage"+ "(96,95,92,90)");

letter6 = checkGrade(96)

console.log("Hello, student, your average is: " + averageVar6  + ". The letter equivalent is " + letter6)